import { createSlice } from '@reduxjs/toolkit'

const sampleSlice = createSlice({
  name: 'sample',
  initialState: 'sample',
  reducers: {
    setSample: (state, action) => action.payload,
  },
})

export const { setSample } = sampleSlice.actions

export default sampleSlice.reducer
